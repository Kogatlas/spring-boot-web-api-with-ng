package com.example.javang;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RestController
public class DemoApplication {
	final String uri = "https://hacker-news.firebaseio.com/v0/";
	
	@RequestMapping("/best")
	public Map<String,Object> GetBest() {
		Map<String,Object> model = new HashMap<String,Object>();
		model.put("ids", new int[1]);
		model.put("content", "HackerNews Best Articles");
		
		RestTemplate restTemplate = new RestTemplate();
		int[] result = restTemplate.getForObject(uri + "beststories.json?print=pretty", int[].class);
		model.put("ids", result);

		return model;
	}

	@RequestMapping("/new")
	public Map<String,Object> GetNew() {
		Map<String,Object> model = new HashMap<String,Object>();
		model.put("ids", new int[1]);
		model.put("content", "HackerNews Best Articles");
		
		RestTemplate restTemplate = new RestTemplate();
		int[] result = restTemplate.getForObject(uri + "newstories.json?print=pretty", int[].class);
		model.put("ids", result);

		return model;
	}

	@RequestMapping("/top")
	public Map<String,Object> GetTop() {
		Map<String,Object> model = new HashMap<String,Object>();
		model.put("ids", new int[1]);
		model.put("content", "HackerNews Best Articles");
		
		RestTemplate restTemplate = new RestTemplate();
		int[] result = restTemplate.getForObject(uri + "topstories.json?print=pretty", int[].class);
		model.put("ids", result);

		return model;
	}

	@RequestMapping("/article/{id}")
	public Map<String,Object> GetById(@PathVariable("id") int id) {
		Map<String,Object> model = new HashMap<String,Object>();
		try {
			model.put("content", "HackerNews Article id: " + id);
			RestTemplate restTemplate = new RestTemplate();
			Article result = restTemplate.getForObject(uri + "item/" + id + ".json", Article.class);
			model.put("result", result);
	
		} catch (final HttpClientErrorException e) {
			System.out.println(e.getStatusCode());
			System.out.println(e.getResponseBodyAsString());
		}

		return model;
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}

class Article {
	private String by;
	public String getby() { return this.by; }
	public void setby(String by) {this.by = by; }
	
	private int descendants;
	public int getdescendants() { return this.descendants; }
	public void setdescendants(int descendants) {this.descendants = descendants; }
	
	private int id;
	public int getid() { return this.id; }
	public void setid(int id) {this.id = id; }
	
	private int score;
	public int getscore() { return this.score; }
	public void setscore(int score) {this.score = score; }
	
	private int time;
	public int gettime() { return this.time; }
	public void settime(int time) {this.time = time; }
	
	private String title;
	public String gettitle() { return this.title; }
	public void settitle(String title) {this.title = title; }
	
	private String type;
	public String gettype() { return this.type; }
	public void settype(String type) {this.type = type; }
	
	private String url;
	public String geturl() { return this.url; }
	public void seturl(String url) {this.url = url; }	
}
