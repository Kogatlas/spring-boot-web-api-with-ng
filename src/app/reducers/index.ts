import { ActionReducerMap, MetaReducer } from '@ngrx/store';

import * as fromArticle from '../entities/article/article.reducer';

export interface State {

  articles: fromArticle.State;
}

export const reducers: ActionReducerMap<State> = {

  articles: fromArticle.reducer,
};


export const metaReducers: MetaReducer<State>[] = [];
