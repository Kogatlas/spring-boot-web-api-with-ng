import 'hammerjs';

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTable, MatTableDataSource } from '@angular/material';
import { Store } from '@ngrx/store';
import { AddArticle, InitArticles, UpdateArticle } from 'src/app/entities/article/article.actions';
import { getAllArticles } from 'src/app/entities/article/article.reducer';

import { Article } from '../../entities/article/article.model';
import * as fromStore from '../../reducers';
import { ArticlesService } from '../shared/articles.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  public counts = {
    best: { load: null, total: null },
    new: { load: null, total: null },
    top: { load: null, total: null }
  };
  public dataSource: MatTableDataSource<Article>;
  public displayedColumns = ['title', 'by'];
  public loaded = false;
  public maxPage: number;
  public pageSize = 5;
  public pageSizeOptions = [5, 10, 25, 50, 100];
  public percentageLoaded = 0;
  public selectedSource = 'best';
  public sources = [ 'best', 'new', 'top' ];
  public cached = {
    best: false,
    new: false,
    top: false,
  };

  private articles$ = [];
  private cachedIds = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<any>;

  constructor(private articlesService: ArticlesService,
              private store: Store<fromStore.State>) { }

  ngOnInit() {
    this.store.dispatch(new InitArticles({ articles: this.articles$ }));
    this.initializeDataSourceComponents();
    this.getTotalArticleCount();
  }

  public filter(value: string) {
    this.dataSource.filter = value.trim().toLocaleLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public openArticle(link: string) {
    window.open(link, '_blank');
  }

  public calculatePercentage(source: string) {
    this.store.select(getAllArticles).subscribe(articles => {
      source = source || this.selectedSource;
      const filtered = articles.filter(article => {
        return article.sources.includes(source);
      });
      this.percentageLoaded = filtered.length / this.counts[this.selectedSource].total * 100;
    });
    this.updatePageSlider();
    if (this.percentageLoaded === 100) {
      this.loaded = true;
      this.percentageLoaded = 0;
    }
  }

  public changePage(value: number) {
    const pageDifference = value - this.paginator.pageIndex;
    for (let i = 0; i !== pageDifference;) {
      if (pageDifference > 0) {
        this.paginator.nextPage();
        i++;
      } else {
        this.paginator.previousPage();
        i--;
      }
    }
  }

  public updatePageSlider() {
    this.maxPage = Math.floor(this.counts[this.selectedSource].load / this.pageSize - 1);
  }

  public sliceDataSource() {
    this.getArticles(this.counts[this.selectedSource].load, this.selectedSource);
    this.updatePageSlider();
  }

  public getSource(source: string) {
    this.cached[source] ? this.getArticles(this.counts[source].load, source)
      : this.getArticlesByCount(Infinity, source);
  }

  private getArticles(count?: number, source?: string) {
    this.store.select(getAllArticles).subscribe(articles => {
      this.articles$ =  articles;
      source = source || this.selectedSource;
      articles = articles.filter(article => {
        return article.sources.includes(source);
      });

      this.dataSource.data = Array.from(articles).reverse().slice(0, count);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  // export class AppComponent {
  //   title = 'Demo';
  //   greeting = {};
  //   constructor(private http: HttpClient) {
  //     http.get('resource').subscribe(data => this.greeting = data);
  //   }
  // }

  private getArticlesByCount(count: number, source?: string) {
    this.articlesService.getArticleIdsByCount(count, source).subscribe((resp: any) => {
      this.loaded = false;
      resp.ids.forEach((id: number) => {
        if (this.cachedIds.includes(id)) {
          const cachedArticle = this.articles$.find(article => article.id === id);
          cachedArticle.sources.push(source);

          this.store.dispatch(new UpdateArticle({
            article: cachedArticle
          }));
        } else {
          this.articlesService.getArticleById(id).subscribe((article: any) => {
            if (article.result) {
              article.result.sources = [];
              article.result.sources.push(source);

              this.store.dispatch(new AddArticle({
                article: article.result
              }));
              this.cachedIds.push(id);
            } else {
              this.counts[source].total--;
              console.log('Article with ID: ' + id + ' not found');
            }
            this.calculatePercentage(source);
          },
          error => {
            console.log('Error:', error);
          });
        }
      });
      this.cached[source] = true;
      this.getArticles();
    });
  }

  private getTotalArticleCount() {
    this.sources.forEach(source => {
      this.articlesService.getArticleIdsByCount(Infinity, source).subscribe((resp: any) => {
        this.counts[source].total = resp.ids.length;
        this.counts[source].load = resp.ids.length;
      });
    });
    this.getArticlesByCount(Infinity, this.selectedSource);
  }

  private initializeDataSourceComponents() {
    this.dataSource = new MatTableDataSource();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
}
