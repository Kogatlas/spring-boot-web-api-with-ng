import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { StoreModule } from '@ngrx/store';
import { of } from 'rxjs';

import { metaReducers, reducers } from '../../reducers';
import { ArticlesService } from './articles.service';

describe('ArticlesService', () => {
  let http: {
    get: jasmine.Spy
  };
  let service: ArticlesService;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule,
      StoreModule.forRoot(reducers, { metaReducers })
    ],
    providers: [
      { provide: HttpClient, useValue: jasmine.createSpyObj('HttpClient', [ 'get' ]) }
    ]
  }));

  beforeEach(() => {
    service = TestBed.get(ArticlesService);
    http = TestBed.get(HttpClient);

    (http.get as jasmine.Spy).and.returnValue(of({rawArticle: {}}));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should make a http get call when getArticleIdsByCount() is invoked', () => {
    service.getArticleIdsByCount(0, 'test');
    expect(http.get).toHaveBeenCalled();
  });

  it('should make a http get call when getArticleById() is invoked', () => {
    // Warning in karma tests thrown by this...
    service.getArticleById(1);
    expect(http.get).toHaveBeenCalled();
  });
});
