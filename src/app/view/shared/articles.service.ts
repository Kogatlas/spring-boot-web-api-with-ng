import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import * as fromStore from '../../reducers';
import { Store } from '@ngrx/store';
import { AddArticle } from 'src/app/entities/article/article.actions';

const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
    'Pragma': 'no-cache' /* Needed for IE11 to not cache calls */
  }),
  withCredentials: true
};
@Injectable({
  providedIn: 'root'
})
export class ArticlesService {
  private articlesUrl = '/api/Articles/';

  constructor(
    private http: HttpClient,
    private store: Store<fromStore.State>
  ) { }

  // CRUD Functions
  public getArticleIdsByCount(count: number, source: string) {
    return this.http.get(source.toLocaleLowerCase());
  }

  public getArticleById(id: number) {
    return this.http.get(`article/${id}`);
  }
}
