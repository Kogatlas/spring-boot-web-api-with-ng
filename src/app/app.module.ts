import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { AppComponent } from './app.component';
import { AppEffects } from './app.effects';
import { ArticleEffects } from './entities/article/article.effects';
import * as fromArticle from './entities/article/article.reducer';
import { metaReducers, reducers } from './reducers';
import { ViewModule } from './view/view.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ViewModule,
    BrowserAnimationsModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot([AppEffects]),
    StoreModule.forFeature('article', fromArticle.reducer),
    EffectsModule.forFeature([ArticleEffects])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
