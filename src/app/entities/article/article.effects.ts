import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, switchMap, tap } from 'rxjs/operators';
import { ArticlesService } from 'src/app/view/shared/articles.service';

import { ArticleActionTypes, InitArticles, LoadArticles } from './article.actions';



@Injectable()
export class ArticleEffects {

  @Effect()
  initArticles$ = this.actions$.pipe(
    ofType('[Article] Init Articles'),
    switchMap((action: InitArticles) => this.articlesService.getArticleIdsByCount(Infinity, 'Best')),
    map(resp => new LoadArticles({articles: []}))
  );


  saveArticle$ = this.actions$.pipe(
    ofType(ArticleActionTypes.AddArticle),
    tap(action => { console.log('Saving action', action); })
  );

  constructor(
    private actions$: Actions,
    private articlesService: ArticlesService
  ) {}

}
